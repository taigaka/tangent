/* tangent web chat v1.1
https://gitlab.com/sanlox/tangent */

var tan_room = "!xxxxxxxxxxxxxxxxxx:matrix.org";

var tan_hs = "https://matrix.homeserver.tld";
var tan_ns = "/_matrix/client/r0/";

var tan_out = document.getElementById("tan_out");
var tan_in = document.getElementById("tan_in");
var tan_user = document.getElementById("tan_user");
var tan_pass = document.getElementById("tan_pass");

var tan_guests = true;

var tan_limit = 10;

/* don't change stuff behind this line
if you don't know what you're doing */

var tan_api = tan_hs + tan_ns;

var tan_token;

var tan_filter = {
  event_fields: [
    "type",
    "content",
    "sender",
    "origin_server_ts"
    ],
  room: {
    rooms: [
      tan_room
    ],
    timeline: {
      limit: tan_limit,
      types: [
        "m.room.message"
      ]
    }
  }
};

function tan_login() {
  var request = {
    type: "m.login.password",
    identifier: {
      type: "m.id.user",
      user: tan_user.value
    },
    password: tan_pass.value,
    initial_device_display_name: "tangent"
  };
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    if(xhr.status == 200) {
      var res = JSON.parse(xhr.responseText);
      tan_token = res["access_token"];
      tan_print("<b>Connected as: " + res["user_id"] + "</b>");
      tan_join();
    } else {
      tan_print("<b>Login failed.</b>");
    }
  };
  xhr.open("POST", tan_api + "login", true);
  xhr.send(JSON.stringify(request));
}

function tan_guest() {
  if(tan_guests) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      if(xhr.status == 200) {
        var res = JSON.parse(xhr.responseText);
        tan_token = res["access_token"];
        tan_print("<b>Connected as " + res["user_id"] + "</b>");
        tan_join();
      } else {
        tan_print("<b>Guest login failed.</b>");
      }
    };
    xhr.open("POST", tan_api + "register?kind=guest", true);
    xhr.send("{}");
  } else {
    tan_print("<b>No guest login allowed.</b>");
  }
}

function tan_logout() {
  if(tan_token) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      if(xhr.status == 200) {
        tan_print("<b>Logout successful.</b>");
      } else {
        tan_print("<b>Logout failed.</b>");
      }
    };
    xhr.open("POST", tan_api + "logout", true);
    xhr.setRequestHeader("Authorization", "Bearer " + tan_token);
    xhr.send("{}");
  } else {
    tan_print("<b>You're not logged in.</b>");
  }
}

function tan_join() {
  if(tan_token) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      if(xhr.status == 200) {
        tan_sync();
      } else if(xhr.status == 403) {
        var res = JSON.parse(xhr.responseText);
        tan_print("<b><a href=\"" + res["consent_uri"] + "\" target=\"_blank\">Accept terms and conditions</a> and <a href=\"javascript:tan_join();\">continue</a></b>");
      } else {
        tan_print("<b>Joining room failed.</b>");
      }
    };
    xhr.open("POST", tan_api + "join/" + encodeURI(tan_room), true);
    xhr.setRequestHeader("Authorization", "Bearer " + tan_token);
    xhr.send("{}");
  } else {
    tan_print("<b>Not logged in.</b>");
  }
}

function tan_sync(batch) {
  if(tan_token) {
    if(batch) {
      var since = "&since=" + batch;
    } else {
      var since = "";
    }
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      if(xhr.status == 200) {
        var res = JSON.parse(xhr.responseText);
        if(res["rooms"]["join"][tan_room]) {
          var events = res["rooms"]["join"][tan_room]["timeline"]["events"];
          for (var i = 0; i < events.length; i++) {
            if(events[i]["content"]["msgtype"] == "m.text") {
              tan_print("<b>[" + tan_time(events[i]["origin_server_ts"]) + "] &lt;" + tan_sanitize(events[i]["sender"]) + "&gt;</b> " + tan_sanitize(events[i]["content"]["body"]));
            }
          }
        }
        tan_sync(res["next_batch"]);
      } else {
        tan_print("<b>Syncing room failed.</b>");
      }
    };
    xhr.open("GET", tan_api + "sync?filter=" + JSON.stringify(tan_filter) + "&timeout=60000" + since, true);
    xhr.setRequestHeader("Authorization", "Bearer " + tan_token);
    xhr.send();
  }
}

function tan_send() {
  if(tan_token) {
    if(tan_in.value) {
      var query = {
        msgtype: "m.text",
        body: tan_in.value
      };
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        tan_in.value = "";
      };
      xhr.open("PUT", tan_api + "rooms/" + tan_room + "/send/m.room.message/" + Date.now(), true);
      xhr.setRequestHeader("Authorization", "Bearer " + tan_token);
      xhr.send(JSON.stringify(query));
      }
    } else {
    tan_print("<b>Not logged in.</b>");
  }
}

function tan_time(stamp) {
  var timestamp = new Date(stamp);
  var hour = timestamp.getHours();
  var minute = timestamp.getMinutes();
  if (hour < 10) {hour = "0" + hour;}
  if (minute < 10) {minute = "0" + minute;}
  return hour + ":" + minute;
}

function tan_sanitize(input) {
	var purge = document.createElement("p");
	purge.textContent = input;
	return purge.innerHTML;
}

function tan_print(output) {
  var newline = document.createElement("p");
  newline.innerHTML = output;
  tan_out.appendChild(newline);
  tan_out.scrollTop = tan_out.scrollHeight;
}
